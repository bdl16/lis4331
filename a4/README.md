# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Assignment 4 Requirements:

*Assignment Requirements:*

1. Include splash screen image, app title, intro text
2. Include appropriate images
3. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned
5. Must add background color(s) or theme
6. Create and displaylauncher icon image

#### README.md file should include the following items:

* Screenshots of splash screen, main screen, invalid screen, valid screen

#### Assignment Screenshots:

| *Screenshot of splash screen*                 | *Screenshot of main screen*                   |
|:---------------------------------------------:|:---------------------------------------------:|
|![Splash](img/splash.png)                      |![Main](img/1.png)                             |

| *Screenshot of invalid entry*                 | *Screenshot of valid entry*                   |
|:---------------------------------------------:|:---------------------------------------------:|
|![Invalid](img/2.png)                          |![Valid](img/3.png)                            |