# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Project 1 Requirements:

*Assignment Requirements:*

1. Create music player application
2. App plays music depending on selection
3. Must include splash screen
4. Must include 3 images w/ buttons to select artist

#### README.md file should include the following items:

* Screenshot of splash screen, main screen, and currently playing screen

#### Assignment Screenshots:

| *Screenshot of splash screen*                 | *Screenshot of main screen*                   | *Screenshot of currently playing screen*      |
|:---------------------------------------------:|:---------------------------------------------:|:---------------------------------------------:|
|![Splash](img/1.png)                           |![Main](img/2.png)                             |![Playing](img/3.png)                          |