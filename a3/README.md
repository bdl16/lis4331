# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Assignment 3 Requirements:

*Assignment Requirements:*

1. Create currency conversion application
2. Converts US dollars to Euros, Pesos, or Canadian dollars
3. Must include radio buttons
4. Must include launcher icon

#### README.md file should include the following items:

* Screenshots of app's two interfaces and toast notification

#### Assignment Screenshots:

| *Screenshot of unpopulated user interface*    | *Screenshot of toast notification*            | *Screenshot of converted currency*            |
|:---------------------------------------------:|:---------------------------------------------:|:---------------------------------------------:|
|![First](img/screen1.png)                      |![Toast](img/toast.png)                        |![Converted](img/result.png)                   |