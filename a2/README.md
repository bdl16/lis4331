# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Assignment 2 Requirements:

*Assignment Requirements:*

1. Create a Tip Calculator application
2. Drop-down menu for total number of guests (including yourself)
3. Drop-down menu for tip percentage (5% increments)
4. Must add background color(s) or theme
5. Must create and displaylauncher icon image

#### README.md file should include the following items:

* Screenshot of running application's unpopulated user interface
* Screenshot of running application's populated user interface

#### Assignment Screenshots:

| *Screenshot of unpopulated user interface*    | *Screenshot of populated user interface*      |
|:---------------------------------------------:|:---------------------------------------------:|
|![First](img/first.png)                        |![Second](img/second.png)                      |