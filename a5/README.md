# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Assignment 5 Requirements:

*Assignment Requirements:*

1. Create RSS Feed App
2. Include splash screen, feed, selection, and article images
3. Must find your own RSS Feed
4. Create and display launcher icon

#### README.md file should include the following items:

* Screenshots of splash screen, main feed, selection, and article

#### Assignment Screenshots:

| *Screenshot of Splash*                        | *Screenshot of Feed*                          |
|:---------------------------------------------:|:---------------------------------------------:|
|![Splash](img/splash.png)                      |![Feed](img/main.png)                          |

| *Screenshot of Selection*                     | *Screenshot of Article*                       |
|:---------------------------------------------:|:---------------------------------------------:|
|![Selection](img/info.png)                     |![Article](img/article.png)                    |