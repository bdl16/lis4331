# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Project 2 Requirements:

*Assignment Requirements:*

1. Create task list app
2. Include splash screen
3. Insert at least five example tasks
4. Test database class
5. Create and display launcher icon

#### README.md file should include the following items:

* Screenshots of running application

#### Assignment Screenshots:

| *Screenshot of Splash Screen*                 | *Screenshot of Task List Activity*            |
|:---------------------------------------------:|:---------------------------------------------:|
|![Splash](img/splash.png)                      |![Task List](img/main.png)                     |