# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### Assignment 1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions
4. Bitbucket repo links: a) this assignment and b) the completed tutorials

|README.md file should include the following items:   |Git commands w/short descriptions:   |
|---|---|
|Screenshot of running java Hello   |git init - Create an empty Git repository or reinitialize an existing one   |
|Screenshot of running Android Studio - My First App   |git status - Show the working tree status   |
|Screenshot of running Android Studio - Contacts App   |git add - Add file contents to the index   |
|git commands with short descriptions   |git commit -  Record changes to the repository   |
|   |git push - Update remote refs along with associated objects   |
|   |git pull - Fetch from and integrate with another repository or a local branch   |
|   |git clone - Clone a repository into a new directory   |
|||

#### Assignment Screenshots:

| *Screenshot of running java Hello*            | *Screenshot of My First App*                  |
|:---------------------------------------------:|:---------------------------------------------:|
|![Java Hello](img/hello.png)                   |![My First App](img/first.png)                 |

| *Screenshot of Contacts App (Main Screen)*    | *Screenshot of Contacts App (Info Screen)*    |
|:---------------------------------------------:|:---------------------------------------------:|
|![Contacts App (Main Screen)](img/main.png)    |![Contacts App (Info Screen)](img/info.png)    |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bdl16/bitbucketstationlocations/ "Bitbucket Station Locations")