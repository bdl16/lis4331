# LIS4331 - Advanced Mobile Application Development

## Benjamin Landerman

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Tip Calculator App
    - Provide screenshots of unpopulated and populated screens
    - Application must include dropdown for number of guests and tip percentage
    - Application must include launcher icon

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create currency conversion application
    - Converts US dollars to Euros, Pesos, or Canadian dollars
    - Must include radio buttons
    - Must include launcher icon
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Include splash screen image, app title, intro text
    - Include appropriate images
    - Must use persistent data: SharedPreferences
    - Widgets and images must be vertically and horizontally aligned
    - Must add background color(s) or theme
    - Create and displaylauncher icon image
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create RSS Feed App
    - Include splash screen, feed, selection, and article images
    - Must find your own RSS Feed
    - Create and display launcher icon

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create music player application
    - App plays music depending on selection
    - Must include splash screen
    - Must include 3 images w/ buttons to select artist

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create task list app
    - Include splash screen
    - Insert at least five example tasks
    - Test database class
    - Create and display launcher icon